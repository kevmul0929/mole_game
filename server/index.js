const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');

const app = express()

// Middleware
app.use(bodyParser.json());
app.use(cors());

// Users API
const users = require('./routes/api/users');
app.use('/api/users', users);

// Scores API
const scores = require('./routes/api/scores');
app.use('/api/scores', scores);

// Handle Production
if(process.env.NODE_ENV === 'production') {
    // Static Folder
    app.use(express.static(__dirname + '/public/'));

    // Handle SPA
    app.get(/.*/, (req, res) => res.sendFile(__dirname + '/public/index.html'));
}

const port = process.env.PORT || 5000;

app.listen(port, () => console.log(`Server started on port ${port}`));