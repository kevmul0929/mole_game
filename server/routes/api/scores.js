const express = require('express');
const mongodb = require('mongodb');
const dotenv = require('dotenv');

const env = dotenv.config().parsed;

const router = express.Router();

// Get Scores
router.get('/', async (req, res) => {
    const scores = await loadScoresCollection();
    res.send(await scores.find({}).toArray());
});

// Add Score
router.post('/', async (req, res) => {
    const scores = await loadScoresCollection();
    await scores.insertOne({
        user: 'auth User',
        score: req.body.score,
        createdAt: new Date()
    });
    res.status(201).send();
});

// Delete Score

async function loadScoresCollection() {
    const client = await mongodb.MongoClient.connect(
        `mongodb+srv://${env.DB_USER}:${env.DB_PASS}@cluster0-9pe3s.mongodb.net/test?retryWrites=true`,
        { useNewUrlParser: true }
    );

    return client.db('cluster0-9pe3s').collection('scores');
}


module.exports = router;