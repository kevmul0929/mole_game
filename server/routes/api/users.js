const express = require('express');
const mongodb = require('mongodb');
const bcrypt = require('bcrypt');
const dotenv = require('dotenv');

const env = dotenv.config().parsed;

const router = express.Router();

// Get Users
router.get('/', async (req, res) => {
    const users = await loadUsersCollection();
    res.send(await users.find({}).toArray());
});

// Add User
router.post('/', async (req, res) => {
    const users = await loadUsersCollection();
    console.log(req.body);
    var hash = bcrypt.hashSync(req.body.password, 10);
    console.log(hash);
    await users.insertOne({
        name: req.body.name,
        email: req.body.email,
        password: hash,
        createdAt: new Date()
    });
    res.status(201).send();
});

// Delete User

async function loadUsersCollection() {
    const client = await mongodb.MongoClient.connect(
        `mongodb+srv://${env.DB_USER}:${env.DB_PASS}@cluster0-9pe3s.mongodb.net/test?retryWrites=true`,
        { useNewUrlParser: true }
    );

    return client.db('cluster0-9pe3s').collection('users');
}


module.exports = router;